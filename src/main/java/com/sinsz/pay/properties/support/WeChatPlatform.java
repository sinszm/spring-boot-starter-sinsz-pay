package com.sinsz.pay.properties.support;

/**
 * 微信平台类型
 * @author chenjianbo
 */
public enum WeChatPlatform {

    /**
     * 开放平台
     */
    OPEN,

    /**
     * 公众平台
     */
    PUBLIC
}
